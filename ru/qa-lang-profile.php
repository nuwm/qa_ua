<?php
/*
	Question2Answer by Gideon Greenspan and contributors
	http://www.question2answer.org/

	Description: Language phrases for user profile page


	This program is free software; you can redistribute it and/or
	modify it under the terms of the GNU General Public License
	as published by the Free Software Foundation; either version 2
	of the License, or (at your option) any later version.

	This program is distributed in the hope that it will be useful,
	but WITHOUT ANY WARRANTY; without even the implied warranty of
	MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
	GNU General Public License for more details.

	More about this license: http://www.question2answer.org/license.php
*/

return array(
	'permit_view_new_users_page' => 'Перегляд сторінки "Нові користувачі"',
	'permit_view_special_users_page' => 'Перегляд сторінки "Виняткові користувачі"',
	'permit_vote_c' => 'Голосувати за коментарі',
	'user_x_disabled_pms' => 'Користувач ^ заборонив особисті повідомлення.',
	'1_chosen_as_best' => ' (1 визнаний кращим)',
	'1_down_vote' => '1 голос ПРОТИ',
	'1_up_vote' => '1 голос ЗА',
	'1_with_best_chosen' => ' (1 з кращих відповідей)',
	'activity_by_x' => 'Активність ^',
	'answers' => 'Відповіді:',
	'answers_by_x' => "Відповіді від ^",
	'bonus_points' => 'Бонусні бали:',
	'comments' => 'Коментарі:',
	'delete_pm_popup' => "Видалити приватне повідомлення",
	'delete_wall_post_popup' => "Видалити повідомлення",
	'extra_privileges' => 'Додаткові привілегії:',
	'gave_out' => 'Його голоси:',
	'my_account_title' => 'Мій профіль',
	'no_answers_by_x' => "Немає відповіді від ^",
	'no_posts_by_x' => 'Немає повідомлень від ^',
	'no_questions_by_x' => "Немає відповідей від ^",
	'permit_anon_view_ips' => 'Перегляд IP анонімних користувачів',
	'permit_close_q' => 'Закриття будь-яких запитань',
	'permit_delete_hidden' => 'Видалення прихованих повідомлень',
	'permit_edit_a' => 'Редагування відповідей',
	'permit_edit_c' => 'Редагування коментарів',
	'permit_edit_q' => 'Редагування запитань',
	'permit_edit_silent' => "\"Тихе\" редагування повідомлень",
	'permit_post_wall' => "Розміщення повідомлень на стіні користувача",
	'permit_view_voters_flaggers' => "Перегляд, хто голосував або помічав повідомлення",
	'permit_flag' => 'Відмітка повідомлень',
	'permit_hide_show' => 'Приховування та відображення повідомлень',
	'permit_moderate' => 'Підтвердження та відхиляння повідомлень',
	'permit_post_a' => 'Додавання відповідей',
	'permit_post_c' => 'Додавання коментарів',
	'permit_post_q' => 'Додавання запитень',
	'permit_recat' => 'Зміна категорії запитань',
	'permit_retag' => 'Зміна тегів запитань',
	'permit_select_a' => 'Вибір відповіді на питання',
	'permit_view_q_page' => 'Перегляд сторінки із питанням',
	'permit_vote_a' => 'Голосування за відповіді',
	'permit_vote_down' => 'Голосування ПРОТИ',
	'permit_vote_q' => 'Голосування за питання',
	'post_wall_button' => 'Добавити повідомлення',
	'post_wall_blocked' => 'Цей користувач заборонив розміщувати повідомлення на стіні',
	'post_wall_empty' => 'Будь-ласка, надрукуйте повідомлення',
	'post_wall_limit' => 'Ви забагато пишете повідомлень — повторіть спробу пізніше',
	'post_wall_must_be_approved' => 'Для розміщення повідомлень на стіні варифікуйте свій обліковий запис. Перейдіть у ^1свій профіль^2.',
	'post_wall_must_login' => 'Будь-ласка, ^1авторизуйтесь^2 або ^3зареєструйтесь^4, перед тим як залишити повідомлення на стіні.',
	'post_wall_must_confirm' => 'будь-ласка, ^5підтвердіть свій email^6, перед тим як залишити повідомлення на стіні.',
	'questions' => 'Запитання:',
	'questions_by_x' => 'Запитання від ^',
	'ranked_x' => ' (місце в рейтингу #^)',
	'received' => 'Голоса за нього:',
	'recent_activity_by_x' => 'Остання активність ^',
	'score' => 'Бали:',
	'send_private_message' => ' - ^1відправити особисте повідомлення^2',
	'set_bonus_button' => 'Оновити бонуси',
	'title' => 'Звання:',
	'user_x' => 'Користувач ^',
	'voted_on' => 'Голосував:',
	'wall_for_x' => "Стіна повідомлень для ^",
	'wall_view_more' => "Переглянути інші повідомлення на стіні...",
	'x_chosen_as_best' => ' (^ обрано кращим)',
	'x_down_votes' => '^ голосів ПРОТИ',
	'x_up_votes' => '^ голосів ЗА',
	'x_with_best_chosen' => ' (^ з кращою відповіддю)',
);
